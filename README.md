# Melon
Customizable Minecraft server written in Java.

## TODO
- [x] Implement a multithreaded TCP server that can handle multiple clients at once
- [x] Add `VarIntReader` and `VarIntWriter` classes for reading/writing `VarInts`
- [ ] Add `VarLongReader` and `VarLongWriter` classes for reading/writing `VarLongs`
- [ ] Add `StringReader` and `StringWriter` classes for reading/writing `Strings`
- [x] Create a base `Packet` class to represent all the packets
- [x] Create a `PacketDecoder` to decode the raw binary data into packets
- [ ] Delegate the work of the `PacketDecoder` to the individual `Packet` subclasses, this will avoid
      code duplication and prevent the code from becoming spaghetti code.
- [ ] Implement enough of the logic so that the clients can **at least** ping the server,
      and have the server display the status.
- [ ] Implement more logic to allow the clients to connect
- [ ] Possibly implement a flat world generator, somehow?
