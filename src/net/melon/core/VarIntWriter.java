package net.melon.core;

import java.io.DataOutputStream;

/**
 * Provides a class for easily writing VarInts.
 * @link https://wiki.vg/Protocol#Without_compression
 */
public class VarIntWriter {
    public DataOutputStream outputStream;

    public VarIntWriter(DataOutputStream outputStream) {
        this.outputStream = outputStream;
    }

    /**
     * Write a single byte to the output stream.
     * @param value The value to write
     * @throws Exception
     */
    private void writeByte(int value) throws Exception {
        this.outputStream.writeByte(value);
    }

    /**
     * Write a VarInt with the given value to the output stream.
     * @param value The value to write
     * @throws Exception
     */
    public void writeVarInt(int value) throws Exception {
        while (value != 0) {
            byte temp = (byte)(value & 0x7f);

            // >>> means the sign bit is shifted with the rest of the number.
            value >>>= 7;
            if (value != 0) {
                temp |= 0x80;
            }
            writeByte(temp);
        }
    }
}
