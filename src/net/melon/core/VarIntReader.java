package net.melon.core;

import java.io.DataInputStream;

/**
 * Provides a class for easily reading VarInts.
 * @link https://wiki.vg/Protocol#Without_compression
 */
public class VarIntReader {
    public DataInputStream inputStream;

    public VarIntReader(DataInputStream inputStream) {
        this.inputStream = inputStream;
    }

    /**
     * Read a single byte from the input stream.
     * @return Returns the byte read.
     * @throws Exception
     */
    private byte readByte() throws Exception {
        return this.inputStream.readByte();
    }

    /**
     * Reads a VarInt and returns the result. To prevent the possibility of a
     * denial-of-service (DoS) attack, the length is a maximum of 5 bytes.
     * @return Returns a VarInt
     * @throws Exception
     */
    public int readVarInt() throws Exception {
        int bytesRead = 0;
        int result = 0;
        byte read = 0;

        do {
            // Read the byte
            read = readByte();
            int value = (read & 0x7f);
            result |= (value << (7 * bytesRead));

            //System.out.println("I: Read is " + read + " and result is " + result);

            bytesRead++;
            if (bytesRead > 5) {
                // VarInt is limited to a maximum of 5 bytes, otherwise
                // it could potentially cause a denial-of-service (DoS) attack.
                throw new RuntimeException("VarInt is too large");
            }
        } while ((read & 0x80) != 0);

        return result;
    }
}
