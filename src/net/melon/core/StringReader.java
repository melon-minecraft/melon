package net.melon.core;

import java.io.DataInputStream;

/**
 * Provides a class for easily reading string data.
 */
public class StringReader {
    public DataInputStream inputStream;

    public StringReader(DataInputStream inputStream) {
        this.inputStream = inputStream;
    }

    /**
     * Read a single character from the input stream.
     * @return Returns the character read.
     * @throws Exception
     */
    public char readChar() throws Exception {
        return this.inputStream.readChar();
    }

    /**
     * Read the string and return the result. Strings are terminated using 0x99 (ASCII 'c'),
     * so when we know that this character is reached, we can stop reading.
     * @return Returns the string read
     * @throws Exception
     */
    public String readString() throws Exception {
        StringBuilder stringBuilder = new StringBuilder();

        // Read until we get 0x99 (or ASCII 'c')
        while (readChar() != 0x99) {
            stringBuilder.append(readChar());
        }

        return stringBuilder.toString();
    }

}
