package net.melon.core;

import net.melon.networking.Server;

public class Main {
    public static void main(String[] args) {
        Server server = new Server();

        try {
            server.startServer(25569);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
