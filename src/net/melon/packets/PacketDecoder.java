package net.melon.packets;

import net.melon.core.VarIntReader;
import net.melon.networking.ConnectionState;
import net.melon.networking.Server;

import java.io.DataInputStream;
import java.util.logging.Logger;

/**
 * Provides a class for decoding fields from an uncompressed Minecraft packet.
 * @link https://wiki.vg/Protocol#Without_compression
 */
public class PacketDecoder {
    public VarIntReader viReader;

    public Server server;
    public Logger logger;

    public PacketDecoder() {
        this.logger = Logger.getLogger(PacketDecoder.class.getName());
    }

    /**
     * Link the server to the packet reader to emit events to it.
     * @param server The server to link.
     */
    public void linkServer(Server server) {
        this.server = server;
    }

    /**
     * Helper function to escape bytes in a byte array.
     * @param data The data to escape
     * @return An escaped string of bytes
     */
    public String escapeBytes(byte[] data) {
        StringBuilder buffer = new StringBuilder();
        for (byte b : data) {
            if (b >= 0x20 && b <= 0x7e) {
                buffer.append((char) b);
            }
            else {
                buffer.append(String.format("\\x%02x", b & 0xFF));
            }
        }
        return buffer.toString();
    }

    public void readPacket(DataInputStream inputStream) throws Exception {
        /**
         * The packet format (without zlib compression) is as follows.
         *
         * +------------+------------+---------------------------------------------------+
         * | Field Name | Field Type |                       Notes                       |
         * +------------+------------+---------------------------------------------------+
         * | Length     | VarInt     | Length of packet data + length of the packet ID   |
         * | Packet ID  | VarInt     |                                                   |
         * | Data       | byte[]     | Depends of the packet ID, see sections below.     |
         * +------------+------------+---------------------------------------------------+
         */

        viReader = new VarIntReader(inputStream);

        // Read the length and the packet ID
        int length = viReader.readVarInt();
        logger.info("Length of the packet: " + length);

        int packetId = viReader.readVarInt();
        logger.info("Packet ID: " + packetId);

        // Read the data
        byte[] data = new byte[length];
        data = inputStream.readNBytes(length);
        logger.info("Data: " + escapeBytes(data));

        if (server.connectionState == ConnectionState.HANDSHAKING) {
            // Packets will be decoded in the HANDSHAKING state
            switch (packetId) {
                case 0x00:
                    HandshakePacket handshakePacket = new HandshakePacket();
                    logger.info("GOT HANDSHAKE PACKET IN HANDSHAKING STATE");

                    try {
                        handshakePacket.decodePacket(data);
                    }
                    catch(Exception e) {
                        e.printStackTrace();
                        //logger.warning(e.);
                    }
                    break;

            }
        }

//        for (byte b : bytes) {
//            System.out.println("BYTE IS " + b);
//        }
    }
}

