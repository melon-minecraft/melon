package net.melon.packets;

/**
 * A base class representing a packet.
 */
public abstract class Packet {
    /**
     * Decode the packet data and return a Packet instance.
     * @param data
     * @return
     */
    //public abstract Packet decodePacket(byte[] data) throws Exception;
}
