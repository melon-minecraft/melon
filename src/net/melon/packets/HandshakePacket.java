package net.melon.packets;

import net.melon.core.VarIntReader;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.logging.Logger;

public class HandshakePacket extends Packet {

    public VarIntReader viReader;
    public Logger logger;

    public int protocolVersion;
    public String serverAddress;
    public int serverPort;
    public int nextState;

    public HandshakePacket() {
        this.registerLogger();
    }

    public void registerLogger() {
        //logger = Logger.getLogger(HandshakePacket.class.getName());
    }

    public void decodePacket(byte[] data) throws Exception {
        ByteArrayInputStream byteInputStream = new ByteArrayInputStream(data);
        DataInputStream dataInputStream = new DataInputStream(byteInputStream);

        viReader = new VarIntReader(dataInputStream);
        int protocolVersion = viReader.readVarInt();

        System.out.println("Protocol version " + protocolVersion);
        //logger.info("Protocol Version: " + protocolVersion);
        //return null;
    }
}
