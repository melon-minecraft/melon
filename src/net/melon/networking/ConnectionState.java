package net.melon.networking;

public enum ConnectionState {
    INITIAL,
    HANDSHAKING,
    STATUS,
    LOGIN,
    PLAY,
}
