package net.melon.networking;

import net.melon.packets.PacketDecoder;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Server for the Melon project.
 */
public class Server {
    public ConnectionState connectionState;
    public ServerSocket serverSocket;
    public PacketDecoder packetDecoder;
    public Logger logger;

    /**
     * Start a server listening for connections on the given port.
     * @param port
     * @throws Exception
     */
    public void startServer(int port) throws Exception {
        // Create a server socket
        serverSocket = new ServerSocket(port, 100, InetAddress.getByName("localhost"));

        // Register a logger
        logger = Logger.getLogger(Server.class.getName());
        logger.info("Starting server on port " + port);

        // Register the packet decoder
        packetDecoder = new PacketDecoder();
        packetDecoder.linkServer(this);

        // The initial connection state will be INITIAL
        connectionState = ConnectionState.INITIAL;

        while (true) {
            // Accept connections
            Socket clientSocket = serverSocket.accept();

            logger.info("Received a connection from " + clientSocket.getInetAddress().getHostName() + ":" + clientSocket.getPort());

            Runnable runnable = () -> handleClient(clientSocket);
            Thread clientThread = new Thread(runnable);
            clientThread.start();
        }
    }

    public void handleClient(Socket socket) {
        try {
            DataInputStream inputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());

            // Assume that a handshake is going to take place, the connection state
            // is now HANDSHAKING.
            connectionState = ConnectionState.HANDSHAKING;

            packetDecoder.readPacket(inputStream);
            //logger.info(dataInput.readFully(byte));
            //logger.info("Received from client: " + reader.readLine());
        }
        catch(Exception e) {
            logger.severe("Exception: " + e.getMessage());
        }
    }
}
